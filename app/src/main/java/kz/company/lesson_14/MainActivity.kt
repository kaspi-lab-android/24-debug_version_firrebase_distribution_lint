package kz.company.lesson_14

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_main.*

private const val GOOGLE_SIGN_IN = 9001

class MainActivity : AppCompatActivity() {

    private var firebaseAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        signInButton.setOnClickListener {
            signIn()
        }

        // firebaseAuth?.signOut()
    }

    private fun signIn() {
        firebaseAuth = FirebaseAuth.getInstance()

        val options = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(this.getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        val googleSignInClient = GoogleSignIn.getClient(this.applicationContext, options)
        val signInIntent = googleSignInClient.signInIntent

        startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GOOGLE_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                val credential = GoogleAuthProvider.getCredential(account?.idToken, null)

                firebaseAuth?.signInWithCredential(credential)
                    ?.addOnCompleteListener { authResult ->
                        if (authResult.isSuccessful) {
                            firebaseAuth?.currentUser?.let { user ->
                                user.getIdToken(true).addOnCompleteListener { result ->
                                    Log.d("GET USER", "SUCCESS")
                                    result.result?.token
                                    nameTextView.text = user.displayName
                                    emailTextView.text = user.email
                                    Glide.with(this).load(user.photoUrl).into(avatarImageView)
                                }
                            }
                        } else {
                            Log.e("SigIn", "Failure signInWithCredential")
                        }
                    }
            } catch (e: ApiException) {
                Log.e(e.toString(), "Google sign in failed")
            }
        }
    }
}
